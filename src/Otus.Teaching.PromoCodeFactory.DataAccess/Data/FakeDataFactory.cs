﻿using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public static class FakeDataFactory
    {
        private static IList<Employee> _employees;
        private static IList<Role> _roles;
        private static IList<Preference> _preferences;
        private static IList<Customer> _customers;
        private static IList<PromoCode> _promoCodes;

        public static IList<Employee> Employees
        {
            get
            {
                if (_employees == null)
                {
                    _employees = new List<Employee>()
                    {
                        new Employee()
                        {
                            Id = Guid.Parse("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"),
                            Email = "owner@somemail.ru",
                            FirstName = "Иван",
                            LastName = "Сергеев",
                            RoleId = Roles.FirstOrDefault(x => x.Name == "Admin").Id,
                            AppliedPromocodesCount = 5
                        },
                        new Employee()
                        {
                            Id = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435895"),
                            Email = "andreev@somemail.ru",
                            FirstName = "Петр",
                            LastName = "Андреев",
                            RoleId = Roles.FirstOrDefault(x => x.Name == "PartnerManager").Id,
                            AppliedPromocodesCount = 10
                        }
                    };
                }
                return _employees;
            }
        }

        public static IList<Role> Roles
        {
            get
            {
                if (_roles == null)
                {
                    _roles = new List<Role>()
                    {
                        new Role()
                        {
                            Id = Guid.Parse("53729686-a368-4eeb-8bfa-cc69b6050d02"),
                            Name = "Admin",
                            Description = "Администратор",
                        },
                        new Role()
                        {
                            Id = Guid.Parse("b0ae7aac-5493-45cd-ad16-87426a5e7665"),
                            Name = "PartnerManager",
                            Description = "Партнерский менеджер"
                        },
                        new Role()
                        {
                            Id = Guid.Parse("4F193E50-D2A5-4DD7-AF04-194416BEFF0F"),
                            Name = "Statistic",
                            Description = "Аналитик"
                        }
                    };
                }
                return _roles;
            }
        }
        
        public static IList<Preference> Preferences
        {
            get
            {
                if (_preferences == null)
                {
                    _preferences = new List<Preference>()
                    {
                        new Preference()
                        {
                            Id = Guid.Parse("ef7f299f-92d7-459f-896e-078ed53ef99c"),
                            Name = "Театр",
                        },
                        new Preference()
                        {
                            Id = Guid.Parse("c4bda62e-fc74-4256-a956-4760b3858cbd"),
                            Name = "Семья",
                        },
                        new Preference()
                        {
                            Id = Guid.Parse("76324c47-68d2-472d-abb8-33cfa8cc0c84"),
                            Name = "Дети",
                        }
                    };
                }
                return _preferences;
            }
        }

        public static IList<Customer> Customers
        {
            get
            {
                if (_customers == null)
                {
                    var customerId = Guid.Parse("a6c8c6b1-4349-45b0-ab31-244740aaf0f0");
                    _customers = new List<Customer>()
                    {
                        new Customer()
                        {
                            Id = customerId,
                            Email = "ivan_sergeev@mail.ru",
                            FirstName = "Иван",
                            LastName = "Петров",
                            Preferences = new List<CustomerPreference>(){new CustomerPreference{CustomerId = customerId, Preference = Preferences[0]} },
                            PromoCodes = new List<PromoCode>(){ PromoCodes[0] }
                        }
                    };
                }
                
                return _customers;
            }
        }

        public static IList<PromoCode> PromoCodes
        {
            get
            {
                if (_promoCodes == null)
                {
                    _promoCodes = new List<PromoCode>()
                    {
                        new PromoCode()
                        {
                            BeginDate = new DateTime(2022, 2, 12),
                            EndDate = new DateTime(2022, 12, 31),
                            Id = Guid.Parse("BBAD65B8-A4C3-45E6-A4C6-3A51E0CEE3D7"),
                            Code = "LETO",
                            PartnerName = "Partner Ivan",
                            PartnerManagerId = Employees.First().Id,
                            PreferenceId = Preferences.First().Id,
                            CustomerId = Guid.Parse("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"),
                        }
                    };
                }

                return _promoCodes;
            }
        }
    }
}