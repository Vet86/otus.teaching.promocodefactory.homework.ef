﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Helpers
{
    public static class ModelConverterExtensions
    {
        public static CustomerResponse ToCustomerResponse(this Customer customer)
        {
            return new CustomerResponse()
            {
                Id = customer.Id,
                Email = customer.Email,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                PromoCodes = customer.PromoCodes?.Select(x => x.ToPromoCodeShortResponse()).ToList(),
                PreferenceShortResponses = customer.Preferences?.Select(x => x.Preference.ToPreferenceShortResponse()).ToList(),
            };
        }

        public static CustomerShortResponse ToCustomerShortResponse(this Customer customer)
        {
            return new CustomerShortResponse()
            {
                Id = customer.Id,
                Email = customer.Email,
                FirstName = customer.FirstName,
                LastName = customer.LastName
            };
        }

        public static PromoCodeShortResponse ToPromoCodeShortResponse(this PromoCode promocode)
        {
            return new PromoCodeShortResponse()
            {
                Id = promocode.Id,
                BeginDate = promocode.BeginDate.ToString(),
                Code = promocode.Code,
                EndDate = promocode.EndDate.ToString(),
                PartnerName = promocode.PartnerName,
            };
        }
        
        public static PreferenceShortResponse ToPreferenceShortResponse(this Preference preference)
        {
            return new PreferenceShortResponse()
            {
                Id = preference.Id,
                Name = preference.Name,
            };
        }

        public static Customer ToCustomer(this CreateOrEditCustomerRequest customer, Guid? id = null)
        {
            return new Customer()
            {
                Id = id ?? Guid.NewGuid(),
                Email = customer.Email,
                FirstName = customer.FirstName,
                LastName = customer.LastName
            };
        }
    }
}
