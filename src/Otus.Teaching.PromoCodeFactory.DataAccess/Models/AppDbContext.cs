﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Models
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {
        }

        public AppDbContext()
        {
        }

        public DbSet<Employee> Employees { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Preference> Preferences { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<PromoCode> PromoCodes { get; set; }
        public DbSet<CustomerPreference> CustomerPreferences { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Data Source=D:\\Sources\\GitLab\\otus.teaching.promocodefactory.homework.ef\\DB\\ApplicationDB.db");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Employee>()
                .HasOne(em => em.Role);
            
            modelBuilder.Entity<Customer>()
                .HasMany(em => em.Preferences);

            modelBuilder.Entity<CustomerPreference>()
                .HasKey(cr => new { cr.CustomerId, cr.PreferenceId });

            modelBuilder.Entity<CustomerPreference>()
                .HasOne(cr => cr.Customer)
                .WithMany(b => b.Preferences)
                .HasForeignKey(cr => cr.CustomerId);

            modelBuilder.Entity<CustomerPreference>()
                .HasOne(cr => cr.Preference)
                .WithMany(p => p.Customers)
                .HasForeignKey(cr => cr.PreferenceId);

            modelBuilder.Entity<PromoCode>()
                .HasOne(em => em.PartnerManager);

            modelBuilder.Entity<PromoCode>()
                .HasOne(em => em.Preference);
        }

    }
}
