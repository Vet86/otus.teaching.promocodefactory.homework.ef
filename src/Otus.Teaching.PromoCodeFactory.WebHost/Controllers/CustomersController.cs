﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Helpers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Customers
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly ICustomersRepository _customersRepository;

        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="customerRepository"></param>
        public CustomersController(ICustomersRepository customerRepository)
        {
            _customersRepository = customerRepository;
        }


        /// <summary>
        /// Get all the customers
        /// </summary>
        /// <returns>Customers</returns>
        [HttpGet]
        public async Task<List<CustomerShortResponse>> GetCustomersAsync()
        {
            var customers = await _customersRepository.GetAllAsync();

            var customersModelList = customers.Select(x => x.ToCustomerShortResponse()).ToList();

            return customersModelList;
        }

        /// <summary>
        /// Customer details
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Customer</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customer = await _customersRepository.GetByIdAsync(id);
            if (customer == null)
                return NotFound();

            var customerResonse = customer.ToCustomerResponse();

            return customerResonse;
        }

        /// <summary>
        /// Create new customer
        /// </summary>
        /// <param name="request"></param>
        /// <returns>Customer</returns>
        [HttpPost]
        public async Task<ActionResult<CustomerResponse>> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            Customer item = request.ToCustomer();
            await _customersRepository.AddAsync(item);
            return item.ToCustomerResponse();
        }

        /// <summary>
        /// Edit customer
        /// </summary>
        /// <param name="id"></param>
        /// <param name="request"></param>
        /// <returns>Customer</returns>
        [HttpPut("{id}")]
        public async Task<ActionResult<CustomerResponse>> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            try
            {
                return (await _customersRepository.EditAsync(id, request.ToCustomer(id))).ToCustomerResponse();
            }
            catch(KeyNotFoundException)
            {
                return NotFound();
            }
        }

        /// <summary>
        /// Delete customer
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Customer</returns>
        [HttpDelete]
        public async Task<ActionResult<CustomerResponse>> DeleteCustomer(Guid id)
        {
            try
            {
                return (await _customersRepository.RemoveAsync(id)).ToCustomerResponse();
            }
            catch (KeyNotFoundException)
            {
                return NotFound();
            }
        }
    }
}