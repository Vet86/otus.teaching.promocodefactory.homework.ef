﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Models;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfRepository<T>
        : IRepository<T>
        where T : BaseEntity
    {
        protected readonly AppDbContext _dataContext;

        public EfRepository(AppDbContext dataContext)
        {
            _dataContext = dataContext;
        }

        public virtual async Task<T> AddAsync(T item)
        {
            await _dataContext.AddAsync(item);
            await _dataContext.SaveChangesAsync();
            return item;
        }

        public virtual async Task<IEnumerable<T>> AddRangeAsync(IEnumerable<T> items)
        {
            await _dataContext.AddRangeAsync(items);
            await _dataContext.SaveChangesAsync();
            return items;
        }

        public virtual async Task<T> EditAsync(Guid id, T newItem)
        {
            var item = await _dataContext.FindAsync<T>(id);
            if (item == null)
                throw new KeyNotFoundException();

            _dataContext.Update(newItem);

            await _dataContext.SaveChangesAsync();
            return newItem;
        }

        public virtual async Task<bool> ExistsAsync(Guid id)
        {
            return await _dataContext.FindAsync<T>(id) != null;
        }

        public virtual async Task<IEnumerable<T>> GetAllAsync()
        {
            return await _dataContext.Set<T>().ToListAsync();
        }

        public virtual async Task<T> GetByIdAsync(Guid id)
        {
            return await _dataContext.FindAsync<T>(id);
        }

        public virtual async Task<T> RemoveAsync(Guid id)
        {
            var item = await GetByIdAsync(id);
            if (item == null)
                throw new KeyNotFoundException();

            _dataContext.Remove(item);

            await _dataContext.SaveChangesAsync();
            return item;
        }
    }
}