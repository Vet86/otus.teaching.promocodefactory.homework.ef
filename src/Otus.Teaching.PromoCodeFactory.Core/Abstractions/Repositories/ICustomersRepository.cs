﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface ICustomersRepository : IRepository<Customer>
    {
        Task<IEnumerable<Customer>> GetCustomersByPreferenceIdAsync(Guid preferenceId);
        Task<Customer> AddNewPromocodeAsync(Guid customerId, PromoCode promoCode);
    }
}