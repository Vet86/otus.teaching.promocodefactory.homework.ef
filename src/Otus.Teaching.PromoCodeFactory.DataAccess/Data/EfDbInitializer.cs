﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Models;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public class EfDbInitializer
            : IDbInitializer
    {
        private readonly AppDbContext _dataContext;

        public EfDbInitializer(AppDbContext dataContext)
        {
            _dataContext = dataContext;
        }

        public void InitializeDb()
        {
            _dataContext.Database.EnsureDeleted();
            _dataContext.Database.EnsureCreated();

            _dataContext.AddRange(FakeDataFactory.Customers);

            _dataContext.AddRange(FakeDataFactory.Roles);

            _dataContext.AddRange(FakeDataFactory.Employees);

            _dataContext.AddRange(FakeDataFactory.PromoCodes);

            _dataContext.AddRange(FakeDataFactory.Preferences);
            _dataContext.SaveChanges();
        }
    }
}
