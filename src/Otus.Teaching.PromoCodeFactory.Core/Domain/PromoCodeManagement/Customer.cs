﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Customer
        :BaseEntity
    {
        [MaxLength(50)]
        [Required]
        public string FirstName { get; set; }
        
        [MaxLength(50)]
        [Required]
        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        [MaxLength(50)]
        [Required]
        public string Email { get; set; }

        public ICollection<CustomerPreference> Preferences { get; set; }

        public ICollection<PromoCode> PromoCodes { get; set; }
    }
}