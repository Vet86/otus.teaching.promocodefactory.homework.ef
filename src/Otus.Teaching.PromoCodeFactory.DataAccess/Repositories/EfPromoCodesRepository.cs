﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Models;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfPromoCodesRepository : EfRepository<PromoCode>, IPromoCodesRepository
    {
        public EfPromoCodesRepository(AppDbContext dataContext) : base(dataContext)
        {
        }

        public override async Task<PromoCode> AddAsync(PromoCode promoCode)
        {
            _dataContext.Customers.Attach(promoCode.Customer);
            //return await base.AddAsync(promoCode);

            await _dataContext.AddAsync(promoCode);
            await _dataContext.SaveChangesAsync();

            return promoCode;
        }
    }
}