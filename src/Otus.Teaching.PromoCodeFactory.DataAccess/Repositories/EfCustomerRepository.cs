﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Models;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfCustomerRepository : EfRepository<Customer>, ICustomersRepository
    {
        public EfCustomerRepository(AppDbContext dataContext) : base(dataContext)
        {
        }

        public override async Task<Customer> GetByIdAsync(Guid id)
        {
            return await _dataContext.Customers
                .Include(x => x.PromoCodes)
                .Include(x => x.Preferences)
                .ThenInclude(x => x.Preference)
                .FirstOrDefaultAsync(x=> x.Id == id);
        }

        public async Task<IEnumerable<Customer>> GetCustomersByPreferenceIdAsync(Guid preferenceId)
        {
            return await _dataContext.Customers
                .Include(x => x.PromoCodes)
                .Include(x => x.Preferences)
                .ThenInclude(x => x.Preference)
                .Where(x=>x.Preferences.Any(r=>r.PreferenceId == preferenceId))
                .ToArrayAsync();
        }

        public async Task<Customer> AddNewPromocodeAsync(Guid customerId, PromoCode promoCode)
        {
            _dataContext.PromoCodes.Add(promoCode);

            await _dataContext.SaveChangesAsync();
            return null;
        }

    }
}