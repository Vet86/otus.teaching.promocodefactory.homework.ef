﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class CustomerResponse
    {
        public Guid Id { get; set; }

        [MaxLength(50)]
        [Required]
        public string FirstName { get; set; }

        [MaxLength(50)]
        [Required]
        public string LastName { get; set; }

        
        [MaxLength(50)]
        [Required]
        public string Email { get; set; }

        public List<PromoCodeShortResponse> PromoCodes { get; set; }

        public List<PreferenceShortResponse> PreferenceShortResponses { get; set; }
    }
}