﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface IRepository<T>
        where T: BaseEntity
    {
        Task<IEnumerable<T>> GetAllAsync();

        Task<T> GetByIdAsync(Guid id);

        Task<T> AddAsync(T item);

        Task<IEnumerable<T>> AddRangeAsync(IEnumerable<T> items);

        Task<T> RemoveAsync(Guid id);

        Task<bool> ExistsAsync(Guid id);

        Task<T> EditAsync(Guid id, T item);
    }
}